<?php


use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var \yii\web\View $this
 * @var \dektrium\user\models\Profile $profile
 */

$this->title = empty($profile->name) ? Html::encode($profile->user->username) : Html::encode($profile->name);
$this->params['breadcrumbs'][] = $this->title;

$this->registerCssFile('/css/profile.css');
?>
<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-6">
        <div class="row">
            <div class="col-sm-6 col-md-4">
                <?= Html::img($avatar, [
                    'class' => 'img-rounded c-user_img',
                    'id' => 'usr_img',
//                    'alt' => $this->params['profile']->username,
                    'width' => 200
                ]) ?>
                <p class="username"><b><?= $model->user->username ?></b></p>
            </div>
            <div class="col-md-offset-1 col-md-7">
                <?= Html::a('Створити запис', Url::to(['/blog/create']), ['class' => 'btn btn-success'])?>

                <?= ListView::widget([
                    'dataProvider' => $dataProvider,
                    'itemView' => '/_blogs',
                ]); ?>
            </div>
        </div>
    </div>
</div>

<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\Menu;
use yii\widgets\ActiveForm;

/**
 * @var dektrium\user\models\User $user
 */

$user = Yii::$app->user->identity;
$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;

$js = <<< JS
    $('#upload_avatar').change(function(){
        var upload = new FormData();
        upload.append('file', $('input[id="upload_avatar"]')[0].files[0]);
        $.ajax({
            cache: false,
            processData: false,
            contentType: false,
            url: '/settings/update-avatar',
            data: upload,
            type: 'POST',
            success: function(data){
                $('#usr_img').attr('src', data);
            },
            error: function(e){
                alert('Error');
            }
        });
    });
JS;
$this->registerJS($js);
?>
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <div class="c-user__photo">
                <?= Html::img($this->params['avatar'], [
                    'class' => 'img-rounded c-user_img',
                    'id' => 'usr_img',
//                    'alt' => $this->params['profile']->username,
                    'width' => 200
                ]) ?>
                <?php if($upload) : ?>
                    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>
                        <?= $form->field($upload, 'uploadImageUser', ['template' => "{label}\n{input}"])->fileInput(['id' => 'upload_avatar']) ?>
                    <?php ActiveForm::end() ?>
                <?php endif ?>
            </div>
        </h3>
    </div>
    <div class="panel-body">
        <?= Menu::widget([
            'options' => [
                'class' => 'nav nav-pills nav-stacked',
            ],
            'items' => [
                ['label' => Yii::t('user', 'Profile'), 'url' => ['/user/settings/profile']],
                ['label' => Yii::t('user', 'Account'), 'url' => ['/user/settings/account']],
                [
                    'label' => Yii::t('user', 'Networks'),
                    'url' => ['/user/settings/networks'],
                    'visible' => $networksVisible
                ],
            ],
        ]) ?>
    </div>
</div>

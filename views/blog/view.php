<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use app\assets\CommentAsset;
use app\assets\GalleryAsset;

CommentAsset::register($this);
GalleryAsset::register($this);

?>

<div class="row">
    <div class="span8">
        <div class="row">
            <div class="span8">
                <h4><strong><?= $blog['title'] ?></strong></h4>
            </div>
        </div>
        <div class="row">
            <span class="col-md-2">
                <a href="#" class="author">
                    <img src="<?= $avatar ?>" alt="" width="200px">
                    <p class="author_post"><b><?= $blog->author->user->username ?></b></p>
                </a>
            </span>
            <span class="col-md-8">
                <?= Html::a('<span class="glyphicon glyphicon-trash"></span>', Url::to(['/blog/delete', 'bid' => $blog->blog_id]), ['title' => Yii::t('app', 'видалити запис')]); ?>
                <?= Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/blog/edit', 'bid' => $blog->blog_id]), ['title' => Yii::t('app', 'редагувати запис')]); ?>
                <p>
                    <?= $blog['text'] ?>
                </p>
            </span>
        </div>
        <div class="col-md-offset-3">
            <?php if(!empty($blog->pictures)) : ?>
                <div class="galleria">
                    <?php foreach($blog->pictures as $picture) : ?>
                        <img src="<?= Url::to(['/web/uploads/images/'.$blog['author_id'].'/'.$picture['src']]) ?>" data-title="My title" data-description="My description">
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>
        </div>
        <hr>
        <?php if($blog['commenting']) : ?>
        <div class="comments">
            <div class="col-md-12">
                <?php if(!empty($blog->comments)) : ?>
                <?php foreach($blog->comments as $comment) :
                    if($comment->answer) : ?>
                       <div class="col-md-8">
                           <?= $comment->text ?>
                       </div>

                    <?php else : ?>
                        <div class="col-md-10">
                            <span class="col-md-2">
                                <div>
                                    <?= $comment->authorCom->user->username ?>
                                </div>
                                <div>
                                    <?= date('F j, Y, H:m', $comment->date_com) ?>
                                </div>
                            </span>
                            <span class="col-md-10">
                                <?= $comment->text ?>
                            </span>

                       </div>
                    <?php endif; ?>
                        <div class="row">
                            <?= Html::a('Відповісти', '#', ['id' => 'add_answer']) ?>
                        </div>

                <?php endforeach; ?>
                <?php else : ?>
                    <div class="col-md-8">
                        <?= Yii::t('app', 'Цей запис ще не коментували') ?>
                    </div>
                <?php endif; ?>
            </div>
            <?php $form = ActiveForm::begin([
                'id' => 'form_comment',
                'options' => ['class' => 'c-profileForm form-horizontal'],
                'fieldConfig' => [
                    'template' => "{input}<div>{error}\n{hint}</div>",
                    'options' => [
                        'tag' => false],
                ],
            ]); ?>
                <?= $form->field($comment, 'text')->textarea(['placeholder' => 'Ваш коментар...']); ?>
                <?= $form->field($comment, 'answer')->textInput(['id'=>'answer','type' => 'hidden']); ?>

                <?= Html::submitButton('Додати', ['class' => 'btn btn-success']) ?>

            <?php ActiveForm::end(); ?>
            <div class="row">
                <?= Html::a('Додати коментар', '#', ['id' => 'add_comment']) ?>
            </div>
        </div>
        <?php else : ?>
            <div class="col-md-8">
                <?= Yii::t('app', 'Для цього запису відключена можливість коментування') ?>
            </div>
        <?php endif; ?>
    </div>
</div>


<?php

use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\FileInput;
//use Yii;

$this->title = 'Створити новий запис';
?>



<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<?= $form->field($model, 'title')->textInput(['placeholder' => 'Заголовок']) ?>
<?= $form->field($model, 'text')->textarea(['rows' => 12, 'placeholder' => 'Зміст']) ?>
<?= $form->field($model, 'images[]')->widget(FileInput::classname(), ['options' => ['accept' => 'image/*', 'multiple' => true]]) ?>
<?= $form->field($model, 'commenting')->checkbox() ?>

<?= Html::submitButton('Зберегти', ['class' => 'btn btn-success']) ?>

<?= Html::a('Повернутись', Url::to(['/blog/view-blog']), ['class' => 'btn btn-default']) ?>

<?php ActiveForm::end(); ?>

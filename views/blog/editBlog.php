<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

$this->title = "Редагування запису";

?>

<?php $form = ActiveForm::begin() ?>

<?= $form->field($blog, 'title')->textInput(['placeholder' => 'Заголовок']) ?>

<?= $form->field($blog, 'text')->textarea(['placeholder' => 'Зміст']) ?>

<?= Html::submitButton('Зберегти зміни', ['class' => 'btn btn-success'])?>

<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Html;
use yii\helpers\Url;

//debug(count($model->comments));
//$date = date('F j, Y, H:m', $model['datetime']);
?>

<div class="row">
    <div class="span8">
        <div class="row">
            <div class="span8">
                <h4><strong><a href="<?= Url::to(['/blog/view', 'bid' => $model['blog_id']])?>"><?= $model['title'] ?></a></strong></h4>
            </div>
        </div>
        <div class="row">
            <span class="col-md-4">
                <a href="#" class="thumbnail">
                    <?php foreach($model->pictures as $picture) :
                        if($picture->promo) : ?>
                            <img src="<?= Url::to(['/web/uploads/images/'.$model['author_id'].'/'.$picture->src])?>" alt="" width="480px">
                        <?php endif;
                    endforeach; ?>
                </a>
            </span>
            <span class="col-md-8">
                <p>
                    <?= $model['text'] ?>
                </p>
                <p><a class="btn" href="<?= Url::to(['/blog/view', 'bid' => $model['blog_id']])?>">Read more</a></p>
            </span>
        </div>
        <div class="row">
            <div class="span8">
                <p></p>
                <p>
                    <i class="icon-user"></i> by <a href="<?= Url::to(['/blog/view-blog', 'id' => $model['author_id']])?>"><?= $model->author->user->username ?></a>
                    | <i class="icon-calendar"></i> <?= date('F j, Y, H:m', $model['datetime']) ?>
                    | <i class="icon-comment"></i> <?= count($model->comments) . " Коментар(ів)" ?>
                </p>
            </div>
        </div>
    </div>
</div>
<hr>

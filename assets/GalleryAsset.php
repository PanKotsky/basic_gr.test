<?php

namespace app\assets;


use yii\web\AssetBundle;
use yii\web\View;

class GalleryAsset extends AssetBundle
{

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $js = [
        'js/galleria/galleria-1.5.7.js'
    ];
    public $jsOptions = ['position' => View::POS_END,
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

}
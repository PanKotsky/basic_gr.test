-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 18 2017 г., 02:02
-- Версия сервера: 5.7.16
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `basicgroup`
--

-- --------------------------------------------------------

--
-- Структура таблицы `blogs`
--

CREATE TABLE `blogs` (
  `blog_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `author_id` int(11) DEFAULT NULL,
  `datetime` int(11) DEFAULT NULL,
  `commenting` int(1) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `blogs`
--

INSERT INTO `blogs` (`blog_id`, `title`, `text`, `author_id`, `datetime`, `commenting`) VALUES
(1, 'Test1', 'Test1', 1, 1505604826, 1),
(4, 'Test2', 'Test2', 1, 1505605146, 1),
(5, 'Test Title', 'Test Description', 1, 1505605330, 1),
(6, 'Test Title1', 'Test Description1', 1, 1505605354, 1),
(7, 'hgtrjhtj6jhujh6tjh', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505640969, 0),
(8, 'Test message', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505641083, 0),
(9, 'Test message', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505641206, 0),
(10, 'Test message', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505641269, 0),
(11, 'Test message', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505641295, 0),
(12, 'Test message', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505641358, 0),
(13, 'efefef', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505641389, 0),
(14, 'Test message title1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Facere, nam omnis eaque aliquid dignissimos obcaecati architecto autem qui tenetur consequuntur nesciunt reiciendis sunt odit dolorum voluptate dolorem, ratione voluptatum quaerat iure optio quam. Consequuntur excepturi voluptate aperiam nostrum, omnis rem, libero vitae molestias ut quae consectetur nulla praesentium explicabo, dolor.', 1, 1505641447, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `com_id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `author_com` int(11) DEFAULT NULL,
  `date_com` int(11) DEFAULT NULL,
  `answer` int(11) DEFAULT NULL,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`com_id`, `blog_id`, `author_com`, `date_com`, `answer`, `text`) VALUES
(1, 14, 1, 1505666133, NULL, 'Test comment'),
(2, 14, 1, 1505666137, NULL, 'Test comment'),
(3, 14, 1, 1505666182, NULL, 'Test comment'),
(4, 14, 1, 1505666198, NULL, 'Test comment2'),
(5, 1, 1, 1505688416, NULL, 'Test comments');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1505414168),
('m140209_132017_init', 1505414176),
('m140403_174025_create_account_table', 1505414177),
('m140504_113157_update_tables', 1505414179),
('m140504_130429_create_token_table', 1505414180),
('m140830_171933_fix_ip_field', 1505414181),
('m140830_172703_change_account_table_name', 1505414181),
('m141222_110026_update_ip_field', 1505414181),
('m141222_135246_alter_username_length', 1505414182),
('m150614_103145_update_social_account_table', 1505414183),
('m150623_212711_fix_username_notnull', 1505414183),
('m151218_234654_add_timezone_to_profile', 1505414184),
('m160929_103127_add_last_login_at_to_user_table', 1505414184);

-- --------------------------------------------------------

--
-- Структура таблицы `pictures`
--

CREATE TABLE `pictures` (
  `pic_id` int(11) NOT NULL,
  `blog_id` int(11) DEFAULT NULL,
  `src` varchar(45) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `promo` int(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pictures`
--

INSERT INTO `pictures` (`pic_id`, `blog_id`, `src`, `author_id`, `promo`) VALUES
(1, 14, 'NASA_36.jpg', 1, 1),
(2, 14, 'NASA_45.jpg', 1, 0),
(3, 14, 'NASA_50.jpg', 1, 0),
(4, 14, 'NASA_52.jpg', 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `firstname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lastname` varchar(200) COLLATE utf8_unicode_ci DEFAULT NULL,
  `src_avatar` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `public_email`, `location`, `website`, `bio`, `firstname`, `lastname`, `src_avatar`) VALUES
(1, 'Ololo', NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, NULL, '', NULL, NULL, NULL, '', '', 'f8b18593cdbb1ce289330560a44e33aa.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `token`
--

INSERT INTO `token` (`user_id`, `code`, `created_at`, `type`) VALUES
(1, '7PEJ9d0z4m-vgFDmhx6MYQTvIFUSt-up', 1505420441, 0),
(2, 'QrTLcxq1BPbz7Z5kTOLupWQAcaA-JHrH', 1505668688, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`) VALUES
(1, 'Test', 'test@test.com', '$2y$12$nLbn3WOZx6/38eehr6qjLuRJZGk4X0KCLARIUd3i9lhG8mcvoCxd2', 'ZmlAAGtDsWu1PFjQPelABLVgrFXPZjJ6', NULL, NULL, NULL, '127.0.0.1', 1505420441, 1505420441, 0, 1505420456),
(2, 'Kockyj', 'pan_kockyj@ukr.net', '$2y$12$6Qk7t078gp6400TPA75t0uDE0i3Ueglpt7mhHeW18zyM3GvsNsj/a', 'aD-6lhD82ibKAFwuR82rzTtJrVQmj3d7', NULL, NULL, NULL, '127.0.0.1', 1505668688, 1505668688, 0, 1505668709);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`blog_id`),
  ADD KEY `fk_blogs_profile_idx` (`author_id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`com_id`),
  ADD KEY `fk_comments_blogs_idx` (`blog_id`),
  ADD KEY `fk_comments_profile_idx` (`author_com`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `pictures`
--
ALTER TABLE `pictures`
  ADD PRIMARY KEY (`pic_id`),
  ADD KEY `fk_pictures_blogs_idx` (`blog_id`),
  ADD KEY `fk_pictures_profile_idx` (`author_id`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `blogs`
--
ALTER TABLE `blogs`
  MODIFY `blog_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `com_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `pictures`
--
ALTER TABLE `pictures`
  MODIFY `pic_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `fk_blogs_profile` FOREIGN KEY (`author_id`) REFERENCES `profile` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `fk_comments_blogs` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`blog_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_comments_profile` FOREIGN KEY (`author_com`) REFERENCES `profile` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `pictures`
--
ALTER TABLE `pictures`
  ADD CONSTRAINT `fk_pictures_blogs` FOREIGN KEY (`blog_id`) REFERENCES `blogs` (`blog_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_pictures_profile` FOREIGN KEY (`author_id`) REFERENCES `profile` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

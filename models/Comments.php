<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "comments".
 *
 * @property integer $com_id
 * @property integer $blog_id
 * @property integer $author_com
 * @property integer $date_com
 * @property integer $answer
 * @property string $text
 *
 * @property Blogs $blog
 * @property Profile $authorCom
 */
class Comments extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['blog_id', 'author_com', 'date_com', 'answer'], 'integer'],
            ['text', 'string'],
            [['blog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogs::className(), 'targetAttribute' => ['blog_id' => 'blog_id']],
            [['author_com'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['author_com' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'com_id' => Yii::t('app', 'Com ID'),
            'blog_id' => Yii::t('app', 'Blog ID'),
            'author_com' => Yii::t('app', 'Author Com'),
            'date_com' => Yii::t('app', 'Date Com'),
            'answer' => Yii::t('app', 'Answer'),
            'text' => Yii::t('app', 'Text')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['blog_id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthorCom()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'author_com']);
    }
}

<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pictures".
 *
 * @property integer $pic_id
 * @property integer $blog_id
 * @property string $src
 * @property integer $author_id
 * @property integer $promo
 *
 * @property Blogs $blog
 * @property Profile $author
 */
class Pictures extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pictures';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pic_id', 'blog_id', 'author_id', 'promo'], 'integer'],
            [['src'], 'string', 'max' => 45],
            [['blog_id'], 'exist', 'skipOnError' => true, 'targetClass' => Blogs::className(), 'targetAttribute' => ['blog_id' => 'blog_id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['author_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pic_id' => Yii::t('app', 'Pic ID'),
            'blog_id' => Yii::t('app', 'Blog ID'),
            'src' => Yii::t('app', 'Src'),
            'author_id' => Yii::t('app', 'Author ID'),
            'promo' => Yii::t('app', 'Promo'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlog()
    {
        return $this->hasOne(Blogs::className(), ['blog_id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'author_id']);
    }
}

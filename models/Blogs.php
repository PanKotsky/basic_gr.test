<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blogs".
 *
 * @property integer $blog_id
 * @property string $title
 * @property string $text
 * @property integer $author_id
 * @property integer $datetime
 * @property integer $commenting
 *
 * @property Profile $author
 * @property Comments[] $comments
 * @property Pictures[] $pictures
 */
class Blogs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blogs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'datetime', 'commenting'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['text'], 'string'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Profile::className(), 'targetAttribute' => ['author_id' => 'user_id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'blog_id' => Yii::t('app', 'Blog ID'),
            'title' => Yii::t('app', 'Title'),
            'text' => Yii::t('app', 'Text'),
            'author_id' => Yii::t('app', 'Author'),
            'datetime' => Yii::t('app', 'Datetime'),
            'commenting' => Yii::t('app', 'Commenting'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Profile::className(), ['user_id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comments::className(), ['blog_id' => 'blog_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPictures()
    {
        return $this->hasMany(Pictures::className(), ['blog_id' => 'blog_id']);
    }
}

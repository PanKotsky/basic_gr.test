<?php

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\BaseFileHelper;
use Yii;

class UploadAvatarForm extends Model
{

    public $uploadImageUser;

    public function rules(){
        return [
          [['uploadImageUser'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif'],
        ];
    }

    public function upload(){
        if($this->validate()){
            $pathToFile = 'uploads/usr/'. Yii::$app->user->identity->getId();


            /* remove previous avatar */
            $files = glob($pathToFile."/*");
            foreach ($files as $file){
                if(!is_file($file)) continue;
                unlink($file);
            }
            /* if not exist directory for current user - create this directory */
            if(!is_dir($pathToFile)){
                BaseFileHelper::createDirectory($pathToFile, 0755);
            }
            $newImageName = md5(rand(0, getrandmax()));
            $this->uploadImageUser->saveAs($pathToFile .'/'. $newImageName . '.' . $this->uploadImageUser->extension);
            return $newImageName . '.' . $this->uploadImageUser->extension;
        }
        else {
            return false;
        }
    }

    public function attributeLabels()
    {
        return [
            'uploadImageUser' => ''
        ];
    }

}
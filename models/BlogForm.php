<?php

namespace app\models;


use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use Yii;

class BlogForm extends Model
{
    public $title;
    public $text;
    public $images;
    public $commenting;

    public function rules() {
        return [
            [['title', 'text'], 'required'],
            ['title', 'string', 'min' => 1, 'max' => 255, 'message' => 'Неприпустима кількість символів'],
            [['images'], 'file', 'extensions' => 'png, jpg', 'maxFiles' => 0],
            ['commenting', 'number', 'min' => 0, 'max' => 1]
        ];
    }

    public function uploads(){
        if ($this->validate()) {
            $pathToFile = 'uploads/images/'. Yii::$app->user->identity->getId();
            if(!is_dir($pathToFile)){
                FileHelper::createDirectory($pathToFile, 0755);
            }
            foreach ($this->images as $file) {
                $file->saveAs($pathToFile . '/' . $file->baseName . '.' . $file->extension);
            }
            return true;
        } else {
            return false;
        }
    }

    public function save(){
        if ($this->validate()) {

            $datetime = time();

            $blog = new Blogs();
//            $blog->attributes = $this;
            $blog->title = $this->title;
            $blog->text = $this->text;
            $blog->author_id = Yii::$app->user->identity->getId();
            $blog->datetime = $datetime;
            $blog->commenting = $this->commenting;

            $blog->save();

            $this->uploads();

            $cnt = 0;
            foreach ($this->images as $file) {
                $image = new Pictures();
                $image->blog_id = $blog->blog_id;
                $image->author_id = Yii::$app->user->identity->getId();
                $image->src = $file->baseName . '.' . $file->extension;
                if($cnt == 0){
                    $image->promo = 1;
                } else {
                    $image->promo = 0;
                }
                $cnt++;

                $image->save();

            }

        }
        else {
            return false;
        }

        return true;

    }



}
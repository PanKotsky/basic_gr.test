<?php
/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 14.09.2017
 * Time: 23:01
 */

namespace app\models;

use dektrium\user\models\Profile as BaseProfile;
use Yii;

class Profile extends BaseProfile
{
//    /**
//     * @property string  $firstname
//     * @property string  $lastname
//     * @property string  $src_avatar
//     */

    public function rules()
    {
        return [
            // firstname rules
            'firstnameTrim'     => ['firstname', 'filter', 'filter' => 'trim'],
            'firstnameLength'   => ['firstname', 'string', 'max'=> 50, 'message' => 'Sorry, but this first name is too long. Maximum length of 50 letters'],
            'firstnamePattern'  => ['firstname', 'match', 'pattern' => '/^[a-zA-Z]+$/', 'message' => 'Sorry, but firstname can\'t contain numbers'],

            //lastname rules
            'lastnameTrim'      => ['lastname', 'filter', 'filter' => 'trim'],
            'lastnameLength'    => ['lastname', 'string', 'max'=> 75, 'message' => 'Sorry, but this last name is too long. Maximum length of 75 letters'],
            'lastnamePattern'   => ['lastname', 'match', 'pattern' => '/^[a-zA-Z]+$/', 'message' => 'Sorry, but lastname can\'t contain numbers'],

            //email rules
            'emailTrim'         => ['public_email', 'filter', 'filter' => 'trim'],
            'emailPattern'      => ['public_email', 'email'],
            'emailUnique'       => ['public_email', 'unique'],

            //avatar rules
//            'avatarPattern'     => [['src_avatar'], 'file', 'extensions' => 'gif, jpg, png', 'maxSize' => 1024*1024*0.5],
            'avatar'            => ['src_avatar', 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * method getDefaultAvatar - return default avatar of user (if user doesn't have avatar)
     * @throws \Exception
     */

    public function getDefaultAvatar() {
        if (!empty($this->src_avatar)) {
            return Yii::$app->request->baseUrl.'/web/uploads/usr/'.$this->user_id .'/'.$this->src_avatar;
        } else {
            return Yii::$app->request->baseUrl.'/web/uploads/usr/default.png';
        }
    }

}
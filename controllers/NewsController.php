<?php

namespace app\controllers;


use app\models\Blogs;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class NewsController extends Controller
{

    public function actionIndex(){

        $news = Blogs::find()->joinWith(['author', 'pictures']);

        $dataProvider = new ActiveDataProvider([
           'query' => $news,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC,
                ]
            ],
        ]);



        return $this->render('index',[
            'dataProvider' => $dataProvider
        ]);
    }
}
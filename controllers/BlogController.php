<?php

namespace app\controllers;


use app\models\BlogForm;
use app\models\Comments;
use app\models\Profile;
use yii\web\UploadedFile;
use app\models\Blogs;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;
use Yii;

class BlogController extends Controller
{

    public function actionViewBlog($id = null){
        if(is_null($id)){
            $id = Yii::$app->user->identity->getId();
        }

        $profile = Profile::findOne(['user_id' => $id]);

        $avatar = $profile->getDefaultAvatar();

        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        $blogs = Blogs::find()->where(['author_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $blogs,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC,
                ]
            ],
        ]);
        $owner = null;

        if(\Yii::$app->user->identity->getId() == $id) {
            $owner = true;
        }

        elseif (\Yii::$app->user->identity->getId() != $id){
            $owner = false;
        }
        return $this->render('viewBlog', [
            'model' => $profile,
            'avatar' => $avatar,
            'dataProvider' => $dataProvider,
            'owner' => $owner
        ]);

    }

    public function actionCreate(){

        $blog = new BlogForm();

        if($blog->load(Yii::$app->request->post())){
            $blog->images = UploadedFile::getInstances($blog, 'images');
            $blog->save();
            $this->redirect(['view-blog']);
        }

        return $this->render('create',[
            'model' => $blog
        ]);

    }

    public function actionView($bid){

        $comment = new Comments;

        if($comment->load(Yii::$app->request->post())){
            $comment->blog_id = $bid;
            $comment->date_com = time();
            $comment->author_com = Yii::$app->user->identity->getId();
            $comment->save();
            $comment->refresh();
        }

        $blog = Blogs::find()->joinWith(['author', 'pictures', 'comments'])->where(['blogs.blog_id' => $bid])->one();

        $avatar = $blog->author->getDefaultAvatar();

        return $this->render('view', [
            'blog' => $blog,
            'comment' => $comment,
            'avatar' => $avatar
        ]);
    }

    public function actionEdit($bid){

        $blog = Blogs::findOne(['blog_id' => $bid]);

        if($blog->load(Yii::$app->request->post())){
            $blog->update();
            $this->redirect(['view-blog']);
        }

        return $this->render('editBlog', [
           'blog' => $blog
        ]);

    }

    public function actionDelete($bid){
        if($bid){
            $blog = Blogs::findOne(['blog_id' => $bid]);
            $blog->delete();
            return true;
        }
        else{
            return false;
        }

    }

}
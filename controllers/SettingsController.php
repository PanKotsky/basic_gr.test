<?php
/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 14.09.2017
 * Time: 22:59
 */

namespace app\controllers;

use dektrium\user\controllers\SettingsController as BaseSettingsController;
use yii\web\UploadedFile;
use app\models\Profile;
use app\models\UploadAvatarForm;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use Yii;

class SettingsController extends BaseSettingsController
{

    public function behaviors(){
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'disconnect' => ['post'],
                    'delete'     => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['profile', 'account', 'networks', 'disconnect', 'delete'],
                        'roles'   => ['@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['confirm'],
                        'roles'   => ['?', '@'],
                    ],
                    [
                        'allow'   => true,
                        'actions' => ['update-avatar'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionProfile()
    {
        $model = $this->finder->findProfileById(\Yii::$app->user->identity->getId());

        if ($model == null) {
            $model = \Yii::createObject(Profile::className());
            $model->link('user', \Yii::$app->user->identity);
        }

        $avatar = $model->getDefaultAvatar();

        Yii::$app->view->params['avatar'] = $avatar;

        $upload = new UploadAvatarForm();

        $event = $this->getProfileEvent($model);

        $this->performAjaxValidation($model);

        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);
        if ($model->load(\Yii::$app->request->post()) && $model->save()) {
            \Yii::$app->getSession()->setFlash('success', \Yii::t('user', 'Your profile has been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('profile', [
            'model' => $model,
            'upload' => $upload
        ]);
    }

    public function actionUpdateAvatar(){
        $userId = \Yii::$app->user->identity->getId();
        $upload = new UploadAvatarForm();
        if(Yii::$app->request->isAjax){
            $upload->uploadImageUser = UploadedFile::getInstanceByName('file');

            if(!$src = $upload->upload()){

            }
            $userProfile = Profile::findOne(['user_id' => $userId]);

//            debug($userProfile);
//            die;

            $userProfile->src_avatar = $src;
            $userProfile->save();

            return '/web/uploads/usr/'.$userId.'/'.$userProfile->src_avatar;
        }
        return false;
    }

}
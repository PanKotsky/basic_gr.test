<?php
/**
 * Created by PhpStorm.
 * User: PanKockyj
 * Date: 14.09.2017
 * Time: 22:58
 */

namespace app\controllers;

use app\models\Profile;
use app\models\Blogs;
use yii\data\ActiveDataProvider;
use dektrium\user\controllers\ProfileController as BaseProfileController;
use yii\web\NotFoundHttpException;

class ProfileController extends BaseProfileController
{


    public function actionShow($id)
    {

        $profile = Profile::findOne(['user_id' => $id]);

        $avatar = $profile->getDefaultAvatar();

        if ($profile === null) {
            throw new NotFoundHttpException();
        }

        $blogs = Blogs::find()->where(['author_id' => $id]);

        $dataProvider = new ActiveDataProvider([
            'query' => $blogs,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'datetime' => SORT_DESC,
                ]
            ],
        ]);

        if(\Yii::$app->user->identity->getId() == $id) {
            return $this->render('show', [
                'model' => $profile,
                'avatar' => $avatar,
                'dataProvider' => $dataProvider
            ]);
        }
        elseif (\Yii::$app->user->identity->getId() != $id){
            return $this->render('profile', [
                'model' => $profile,
                'avatar' => $avatar,
                'dataProvider' => $dataProvider
            ]);
        }
    }

}